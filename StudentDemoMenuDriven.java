package practice.MenuDrivenStudentComparisonn;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class StudentDemoMenuDriven {

        public static void main(String[] args) {

            ArrayList<StudentDemo> arrayList = new ArrayList<>();
            arrayList.add(new StudentDemo(2, "B", "Mumbai"));
            arrayList.add(new StudentDemo(5, "E", "Pune"));
            arrayList.add(new StudentDemo(1, "C", "Delhi"));
            arrayList.add(new StudentDemo(3, "A", "Goa"));
            arrayList.add(new StudentDemo(4, "D", "Mumbai"));


            System.out.println("1 For the same insertion order");
            System.out.println("2 For sorting name wise");
            System.out.println("3 For sorting city wise");
            System.out.println("4 For sorting roll no wise");

            while (true) {
                System.out.println("Choose the option");
                Scanner sc = new Scanner(System.in);
                int op = sc.nextInt();
                switch (op) {
                    case 1:
                        for (StudentDemo s : arrayList) {
                            System.out.println(s);
                        }
                        break;

                    case 2:
                        System.out.println("Sort by name");
                        Collections.sort(arrayList, new SortByName());
                        for (StudentDemo s : arrayList) {
                            System.out.println(s);
                        }
                        break;

                    case 3: {
                        System.out.println("Sort by CityName");
                        Collections.sort(arrayList, new SortByCityName());
                        for (StudentDemo s : arrayList) {
                            System.out.println(s);
                        }
                        break;
                    }

                    case 4:
                        System.out.println("Sort by Rollno");
                        Collections.sort(arrayList, new SortByRollNo());
                        for (StudentDemo s : arrayList) {
                            System.out.println(s);
                        }
                        break;


                }

            }
        }


}

