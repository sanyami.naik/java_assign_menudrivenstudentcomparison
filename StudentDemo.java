package practice.MenuDrivenStudentComparisonn;

import java.util.Comparator;

public class StudentDemo {
    int rollNo;
    String name;
    String city;

    public StudentDemo(int rollNo, String name, String city) {
        this.rollNo = rollNo;
        this.name = name;
        this.city = city;
    }

    @Override
    public String toString() {
        return "StudentDemo{" +
                "rollNo=" + rollNo +
                ", name='" + name + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}

    class SortByCityName implements Comparator<StudentDemo> {

        @Override
        public int compare(StudentDemo s1,StudentDemo s2) {
            return s1.city.compareTo(s2.city);
        }
    }

class SortByCityNamee implements Comparable<StudentDemo> {


    @Override
    public int compareTo(StudentDemo o) {
        return 0;
    }
}

    class SortByName implements Comparator<StudentDemo> {

        @Override
        public int compare(StudentDemo s1,StudentDemo s2) {
            return s1.name.compareTo(s2.name);
        }
    }

    class SortByRollNo implements Comparator<StudentDemo> {

        @Override
        public int compare(StudentDemo s1,StudentDemo s2) {
            return s1.rollNo-s2.rollNo;
        }
}



